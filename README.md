Programmering i matematiken - med Python
==============
En lärobok för gymansiekurserna Ma1c, Ma2c och Ma3c, av Krister Trangius och Emil Hall. Utgiven på [Thelin Förlag](https://www.thelinab.se/thelin-forlag/). Stöd oss gärna genom att köpa boken via [Skolportalen](https://www.skolportalen.se).

Detta repo har flyttat till github: https://github.com/trangiusarien/pim_python.
